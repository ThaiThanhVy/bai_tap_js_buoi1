/**
 * Input 
 * Nhập vào 5 số thực bất kì : 2, 3, 4, 5, 6.
 * 
 * todo
 * tạo biến a, b, c, d, e
 * tạo hàm sum để cộng 5 số thực lại với nhau 
 * tạo hàm gttb để tính giá trị trung bình bằng cách / 5
 * 
 * output
 * Tính được gttb = 4
 * 
 * 
 */

var a, b, c, d, e;

a = 2;
b = 3;
c = 4;
d = 5;
e = 6;

sum = a+b+c+d+e;
gttb = sum / 5;

console.log("gttb ", gttb);