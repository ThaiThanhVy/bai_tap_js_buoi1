/**
 * Input 
 * Lương 1 ngày là 100000
 * Cho người nhập vào số ngày làm
 * 
 * Todo 
 * Tạo 2 ô input cho người dùng nhập vào , nút button
 * Tiền lương = lương 1 ngày * số ngày làm
 * Thêm thẻ p để hiện lên số tiền lương tháng
 * 
 * 
 * Output
 * In ra màn hình số tiền lương
 * 
 */

function tinhtien() {
    var luong1Ngay = 100000;

    var soNgayLam = document.getElementById("txt-so-ngay-lam").value * 1;

    console.log(soNgayLam , luong1Ngay);

    tienLuong = luong1Ngay * soNgayLam;
    
    document.getElementById("tienluong").innerHTML = `<h1>Số tiền lương tháng này của bạn là: ${tienLuong} đồng</h1>`
}